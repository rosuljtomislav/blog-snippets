import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import numpy as np
import os

#define problem-specific constants
MTOW = 2984 #kg
#max fuel 891 kg

f = 0.625 #empty weight fraction of TBM700
g = 9.81 #m/s^2
LoD = 13.2 #lift to drag ratio
eta_i = 0.99
eta_m = 0.93
eta_p = 0.90
eta_g = 0.98
c_b = 1444000 # battery specific energy (J/kg)
c_p = 0.408 / 1000 / 60 / 60 # PT6A SFC (kgfuel/W*s)

class nf(float):
    def __repr__(self):
        str = '%.1f' % (self.__float__(),)
        if str[-1] == '0':
            return '%.0f' % self.__float__()
        else:
            return '%.1f' % self.__float__()


def W_pay(phi,psi):
	w_pay = (1-f)*MTOW*(1-phi-psi)
	return w_pay
	
def R_tot(phi,psi):
	r_tot = LoD*eta_i*eta_m*eta_p/g*((phi*c_b*(1-f))/(1-psi*(1-f))+eta_g/c_p*np.log(1/(1-psi*(1-f))))
	return r_tot
	
fmt = '%r'

phis = np.linspace(0,1,101)
psis = np.linspace(0,0.7,71)

W_pays = []
R_tots = []
phis_vec = []
psis_vec = []

for i, phi in enumerate(phis):
	for j, psi in enumerate(psis):
		if phi + psi <= 1:
			phis_vec.append(phi)
			psis_vec.append(psi)
			W_pays.append(W_pay(phi,psi))
			R_tots.append(R_tot(phi,psi)/1000)
		else:
			pass
			
#define grid
xi = np.linspace(0,1,200)
yi = np.linspace(0,0.7,200)
z1 = griddata(phis_vec,psis_vec,W_pays,xi,yi,interp='linear')
z2 = griddata(phis_vec,psis_vec,R_tots,xi,yi,interp='linear')
levels_R = [400,800,1200,1600,2000,2400,2800,3200]
levels_W = [10,200,400,600,800]
c1 = plt.contour(xi,yi,z1,levels=levels_W,linewidths=1.2,colors='k',label='Payload')
c1.levels = [nf(val) for val in c1.levels]
plt.clabel(c1,c1.levels,inline=True,fmt=fmt,fontsize=10)
c2 = plt.contour(xi,yi,z2,levels=levels_R,linewidths=2,colors='r',label='Range')
c2.levels = [nf(val) for val in c2.levels]
plt.clabel(c2,c2.levels,inline=True,fmt=fmt,fontsize=10)
#plt.scatter(phis_vec,psis_vec,marker='o',c='b',s=5)
plt.xlim(0,1)
plt.ylim(0,.7)
plt.xlabel(r'$\phi$ - Load fraction batteries')
plt.ylabel(r'$\psi$ - Load fraction fuel')
plt.annotate('Payload (kg)',(.8,.65),color='k')
plt.annotate('Range (km)',(0.8,0.6),color='r')
plt.title('Payload-Range Curve for Hybrid \n Turboelectric Aircraft Similar to TBM-700')
plt.show()

xi = np.linspace(0,3400,200) #range
yi = np.linspace(0,800,200) #payload
z3 = griddata(R_tots,W_pays,phis_vec,xi,yi,interp='linear')
z4 = griddata(R_tots,W_pays,psis_vec,xi,yi,interp='linear')
levels_phi = [0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
levels_psi = [0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.69]
c3 = plt.contour(xi,yi,z3,levels=levels_phi,linewidths=1.2,colors='k',label='phi')
c3.levels = [nf(val) for val in c3.levels]
plt.clabel(c3,c3.levels,inline=True,fmt=fmt,fontsize=10)
c4 = plt.contour(xi,yi,z4,levels=levels_psi,linewidths=2,colors='r',label='psi')
c4.levels = [nf(val) for val in c4.levels]
plt.clabel(c4,c4.levels,inline=True,fmt=fmt,fontsize=10)
#plt.scatter(R_tots,W_pays,marker='o',c='b',s=5)
plt.xlim(0,3400)
plt.ylim(0,800)
plt.xlabel(r'Range (km)')
plt.ylabel(r'Payload (kg)')
plt.annotate('$\phi$ - battery fraction',(2000,700),color='k')
plt.annotate('$\psi$ - fuel fraction',(2000,625),color='r')
plt.title('Payload-Range Curve for Hybrid \n Turboelectric Aircraft Similar to TBM-700')
plt.show()


