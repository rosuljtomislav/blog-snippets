import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import numpy as np
import os

#define problem-specific constants
MTOW = 2984 #kg
#max fuel 891 kg

f = 0.625 #empty weight fraction of TBM700
g = 9.81 #m/s^2
LoD = 13.2 #lift to drag ratio
eta_i = 0.99
eta_m = 0.93
eta_p = 0.90
eta_g = 0.98
c_b = 1444000 # battery specific energy (J/kg)
c_p = 0.408 / 1000 / 60 / 60 # PT6A SFC (kgfuel/W*s)

class nf(float):
    def __repr__(self):
        str = '%.1f' % (self.__float__(),)
        if str[-1] == '0':
            return '%.0f' % self.__float__()
        else:
            return '%.1f' % self.__float__()
	
def R_f(w_pay,eps):
	log_interior = (eps+(1-eps)*c_b*c_p)*MTOW/(eps*MTOW+(1-eps)*c_b*c_p*(w_pay+f*MTOW))
	r_f = LoD*eta_i*eta_m*eta_p*eta_g/c_p/g*np.log(log_interior)
	return r_f

def R_e(w_pay,eps):
	r_e = eps*c_b*LoD*eta_i*eta_m*eta_p/g*((1-f)*MTOW-w_pay)/(eps*MTOW+(1-eps)*c_b*c_p*(w_pay+f*MTOW))
	return r_e

def R_tot(w_pay,eps):
	r_tot = R_f(w_pay,eps)+R_e(w_pay,eps)
	return r_tot
	
def W_f(w_pay,eps):
	w_f = (1-eps)*c_b*c_p*((1-f)*MTOW-w_pay)/(eps+(1-eps)*c_b*c_p)
	return w_f

fmt = '%r'

epses = np.linspace(0,1.1,111)
payloads = np.linspace(0,891,101)

R_tots = []
eps_flat = []
pay_flat = []
w_fs = []

for i, eps in enumerate(epses):
	for j, payload in enumerate(payloads):
			R_tots.append(R_tot(payload,eps)/1000)
			eps_flat.append(eps)
			pay_flat.append(payload)
			w_fs.append(W_f(payload,eps))
			
			
xi = np.linspace(0,3800,200) #range
yi = np.linspace(0,800,200) #payload
z3 = griddata(R_tots,pay_flat,eps_flat,xi,yi,interp='linear')
levels_eps = [0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.99]
c3 = plt.contour(xi,yi,z3,levels=levels_eps,linewidths=1.2,colors='k',label='eps')
c3.levels = [nf(val) for val in c3.levels]
plt.clabel(c3,c3.levels,inline=True,fmt=fmt,fontsize=10)

z4 = griddata(R_tots,pay_flat,w_fs,xi,yi,interp='linear')
levels_wf = [891]
c4 = plt.contour(xi,yi,z4,levels=levels_wf,linewidths=1.2,colors='r',label='fuel')
c4.levels = [nf(val) for val in c4.levels]
plt.clabel(c4,c4.levels,inline=True,fmt=fmt,fontsize=10)

plt.xlim(0,3800)
plt.ylim(0,800)
plt.xlabel(r'Range (km)')
plt.ylabel(r'Payload (kg)')
plt.annotate('$\epsilon$ - degree of hybridization',(2000,700),color='k')
plt.annotate('Fuel volume limit',(2000,650),color='r')

print max(w_fs)
print min(w_fs)

plt.title('Payload-Range Curve for Hybrid \n Turboelectric Aircraft Similar to TBM-700')
plt.show()




